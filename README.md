# **README** #
Welcome to AlchemicalArrows' online BitBucket repository. If you are here, you are more than likely interested in viewing the source code for this project. Before entering the source code section, please be aware that this project is licensed under the General Public License version 3. Please view copyright information about this project below

## **Pull Requests** ##
As of version 2.0, this project is now open sourced, and free for other developers to improve. In order to create a pull request, you will see a tab on the left-hand side of the screen titled "Create pull request". Follow the steps from there, and you are free to make a pull request for this software. If I so chose to accept the pull request, your code will be used in the project for a future version of the software.

## **AlchemicalArrows API** ##
If you were using the API from previous versions, you will know that it is very different. You may have found extreme difficulty with the rigidity of the previous API, and this is the reason a rewrite was in order. The new API for AlchemicalArrows 2 will allow for custom external arrows to be created in a much easier way. If you are looking for documentation on the projects API, please [click here](https://bitbucket.org/2008Choco/alchemicalarrows/wiki/Home), or click "Wiki" on the left-hand side of the screen to view the BitBucket Wiki page

## **Any Questions** ##
If you are to have any questions about the new API, or anything about the plugin, do not hesitate to ask me a question over a private message on the [Bukkit](dev.bukkit.org/profiles/2008Choco/) or [SpigotMC](www.spigotmc.org) website. If you are to have issues with the software (Which includes bugs, improvements, etc.), please leave a ticket on the [Bukkit Ticket Tracker](dev.bukkit.org/bukkit-plugins/alchemical-arrows/tickets) page

## **Copyright** ##
Copyright (C) 2016 Parker Hawke

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. See http://www.gnu.org/licenses/